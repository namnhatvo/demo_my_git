git init: khởi tạo repository local
git config --global user.name:  ghi cấu hình tên người dùng
git config --global user.email:  ghi cấu hình email người dùng
git add -A: Chọn toàn bộ code muốn đẩy lên repo
git commit -m"(message)": xác nhận
git push -u origin (master): đẩy 1 file mới tạo lần 1 ( git push --set-upstream origin)
git remote add origin https://gitlab.com/namnhatvo/demo_my_git.git : kết nối giữa repo local và repo remote
git push: đẩy lên repo
git branch: đứng ở nhánh nào
git checkout -b login: tạo nhánh mới có tên login
git checkout master: chuyển sang nhánh master
muốn chuyển nhánh thì phải commit trước
để đẩy code từ local lên repo cần 3 bước: chọn, xác nhận, đẩy
git merge login: lấy tất cả file của login về
